FROM python:3.8-alpine
LABEL maintainer="Alexander Moreno Delgado <alexander.moreno@0x365.com>"

ENV MYCLI_VERSION 1.21.1

RUN set -e \
    && ln -sf /usr/share/zoneinfo/Etc/UTC /etc/localtime \
	  && apk add --no-cache \
		  build-base \
      libffi-dev \
      openssl-dev \
      python3-dev \
      py3-pip \
    && pip3 install --upgrade \
      mycli==${MYCLI_VERSION} \
    && apk --purge -v del \
      build-base \
      py3-pip \
      python3-dev \
      libffi-dev \
      openssl-dev \
      py3-pip \
    && rm -f /var/cache/apk/*

ENTRYPOINT ["/usr/local/bin/mycli"]
CMD ["--help"]
