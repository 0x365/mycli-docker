# mycli-docker
mycli in a tiny Docker image powered by Alpine Linux

# How to use this image

Print help:

```bash
docker run --rm us.gcr.io/x365-pipes/tools/mycli --help
```

Run MySQL server if you don't have it:

```bash
docker run -d --name=mysql \
  -p 3306:3306 \
  -e MYSQL_ROOT_PASSWORD=secret \
  mysql:5.7
```

Run mycli:

```bash
docker run --rm -ti --name=mycli \
  --link=mysql:mysql \
  us.gcr.io/x365-pipes/tools/mycli \
  --host=mysql \
  --database=mysql \
  --user=root \
  --password=secret
```

Run on Cloud Run
```
gcloud run deploy db-test --image=us.gcr.io/x365-pipes/tools/mycli --region=${REGION} --cpu=1 --memory=128 --platform=managed --project=${GCP_PROJECT} --add-cloudsql-instances=${GCP_CLOUD_SQL_INSTANCE_NAME} --command="/usr/local/bin/mycli" --args "-S","/cloudsql/${INSTANCE_CONNECTION}","-u","${USER}","-p${PASSWORD}","-e","SHOW DATABASES"
```